#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>
#include "thread_safe_queue.hpp"

using namespace std;

void background_work(int id)
{
    cout << "BW#" << id << " start..." << endl;
    this_thread::sleep_for(chrono::milliseconds(rand() % 10000));
    cout << "BW#" << id << " is finished..." << endl;
}

using Task = std::function<void()>;

class ThreadPool
{
    ThreadSafeQueue<Task> tasks_;
    std::vector<std::thread> threads_;
    const static nullptr_t end_of_work_;
public:
    ThreadPool(size_t size)
    {
        for(size_t i = 0; i < size; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            tasks_.push(end_of_work_);

        for(auto& thd : threads_)
            thd.join();
    }

    template <typename Callable>
    auto submit(Callable&& task) -> future<decltype(task())>
    {
        using result_type = decltype(task());

        auto pt = make_shared<packaged_task<result_type()>>(forward<Callable>(task));
        auto future_result = pt->get_future();

        tasks_.push([pt] { (*pt)();});

        return future_result;
    }

    void submit(nullptr_t) = delete;

private:
    void run()
    {
        while(true)
        {
            Task task;
            tasks_.pop(task);

            if (task == end_of_work_)
                break;

            task(); // running a task in pool's thread
        }
    }
};

int calculate_square(int x)
{
    cout << "Starting calculation for " << x << endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    if (x == 13)
        throw std::runtime_error("Error#13");

    return x * x;
}

int main()
{
    ThreadPool thread_pool{8};

    thread_pool.submit([] { background_work(1); });
    thread_pool.submit([] { background_work(2); });
    // thread_pool.submit(nullptr); // compilation error

    for(int i = 3; i <= 30; ++i)
        thread_pool.submit([i] { background_work(i); });


    vector<future<int>> future_squares;

    for(int i = 1; i <= 20; ++i)
        future_squares.push_back(thread_pool.submit([i] { return calculate_square(i); }));

    cout << "\nSquares:\n";

    for(auto& fs : future_squares)
    {
        try
        {
            cout << fs.get() << endl;
        }
        catch(const runtime_error& e)
        {
            cout << "Caught exception: " << e.what() << endl;
        }
    }
}
