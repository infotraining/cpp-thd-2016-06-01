#include <celero/Celero.h>

#include <atomic>
#include <mutex>
#include <thread>
#include <random>
#include <iostream>

CELERO_MAIN

using namespace std;

long calc_hits(long n)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    long counter = 0;

    for (long i = 0 ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }

    return counter;
}

void calc_hits_by_ref1(long n, long& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (long i = 0 ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            hits++;
    }
}

void calc_hits_by_ref1_with_mutex(long n, long& hits, mutex& mtx)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (long i = 0 ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
        {
            lock_guard<mutex> lk{mtx};
            hits++;
        }
    }
}

void calc_hits_by_ref1_with_atomic(long n, atomic<long>& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (long i = 0 ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
        {
            hits.fetch_add(1, memory_order_relaxed);
        }
    }
}

void calc_hits_by_ref2(long n, long& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    long counter = 0;

    for (long i = 0 ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }

    hits += counter;
}


const int N = 10'000'000;


BASELINE(Pi, SingleThreaded, 1, 10)
{
    auto pi = (static_cast<double>(calc_hits(N))/double(N)) * 4;
}

BENCHMARK(Pi, Unsafe, 1, 10)
{
    long counter = 0;
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = N / hardware_threads_count;

    vector<thread> threads;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&counter, no_of_throws] { calc_hits_by_ref1(no_of_throws, counter);});
    }

    for(auto& thd : threads)
        thd.join();

    auto pi = (static_cast<double>(counter) / N) * 4;
}

BENCHMARK(Pi, ThreadSafeWithMutex, 1, 10)
{
    long counter = 0;
    mutex mtx;

    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = N / hardware_threads_count;

    vector<thread> threads;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&counter, &mtx, no_of_throws] {
            calc_hits_by_ref1_with_mutex(no_of_throws, counter, mtx);
        });
    }

    for(auto& thd : threads)
        thd.join();

    auto pi = (static_cast<double>(counter) / N) * 4;
}

BENCHMARK(Pi, ThreadSafeWithAtomic, 1, 10)
{
    atomic<long> counter{};

    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = N / hardware_threads_count;

    vector<thread> threads;

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&counter, no_of_throws] {
            calc_hits_by_ref1_with_atomic(no_of_throws, counter);
        });
    }

    for(auto& thd : threads)
        thd.join();

    auto pi = (static_cast<double>(counter) / N) * 4;
}



BENCHMARK(Pi, Multithreaded_1, 1, 10)
{
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = N / hardware_threads_count;

    vector<thread> threads;
    vector<long> hits(hardware_threads_count);

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, i, no_of_throws] { hits[i] = calc_hits(no_of_throws);});
    }

    for(auto& thd : threads)
        thd.join();

    auto pi = (accumulate(hits.begin(), hits.end(), 0.0) / N) * 4;
}

BENCHMARK(Pi, Multithreaded_2, 1, 10)
{
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = N / hardware_threads_count;

    vector<thread> threads;
    vector<long> hits(hardware_threads_count);

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, i, no_of_throws] { calc_hits_by_ref1(no_of_throws, hits[i]);});
    }

    for(auto& thd : threads)
        thd.join();

    auto pi = (accumulate(hits.begin(), hits.end(), 0.0) / N) * 4;
}

BENCHMARK(Pi, Multithreaded_3, 1, 10)
{
    auto hardware_threads_count = max(thread::hardware_concurrency(), 1u);
    auto no_of_throws = N / hardware_threads_count;

    vector<thread> threads;
    vector<long> hits(hardware_threads_count);

    for(unsigned int i = 0; i < hardware_threads_count; ++i)
    {
        threads.emplace_back([&hits, i, no_of_throws] { calc_hits_by_ref2(no_of_throws, hits[i]);});
    }

    for(auto& thd : threads)
        thd.join();

    auto pi = (accumulate(hits.begin(), hits.end(), 0.0) / N) * 4;
}
