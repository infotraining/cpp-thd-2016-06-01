#include <iostream>
#include <thread>
#include <mutex>

//class Square
//{
//    int size;
//    mutable bool cache_is_valid = false;
//    mutable int cached_area_;

//public:
//    void set_size(int new_size)
//    {
//        if (new_size != size)
//        {
//            cache_is_valid = false;
//            size = new_size;
//        }
//    }

//    int area() const
//    {
//        if (!cache_is_valid)
//        {
//            cached_area_ = size * size;
//            cache_is_valid = true;
//        }

//        return cached_area_;
//    }
//};

class BankAccount
{
    using mutex_type = std::recursive_mutex;

    const int id_;
    double balance_;
    mutable mutex_type mtx_;


public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #"
                  << id_ << "; Balance = "
                  << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<mutex_type> lk_from{mtx_, std::defer_lock};
        std::unique_lock<mutex_type> lk_to{to.mtx_, std::defer_lock};

        std::lock(lk_from, lk_to);

        withdraw(amount);
        to.deposit(amount);
    }

    void withdraw(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<mutex_type> lk{mtx_};
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

void make_deposit(int counter, BankAccount& ba1, double amount)
{
    std::lock_guard<BankAccount> lk{ba1};

    for(int i = 0; i < counter; ++i)
        ba1.deposit(amount);
}

void make_withdraw(int counter, BankAccount& ba1, double amount)
{
    std::lock_guard<BankAccount> lk{ba1};

    for(int i = 0; i < counter; ++i)
        ba1.withdraw(amount);
}

void make_transfer(int counter, BankAccount& from, BankAccount& to, double amount)
{
    for(int i = 0; i < counter; ++i)
        from.transfer(to, amount);
}

int main()
{
    using namespace std;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    const int no_of_operations = 1'000'000;

    thread thd1{&make_deposit, no_of_operations, ref(ba1), 1.0};
    thread thd2{&make_withdraw, no_of_operations, ref(ba1), 2.0};
    thread thd3{&make_transfer, no_of_operations, ref(ba2), ref(ba1), 1.0};
    thread thd4{&make_transfer, no_of_operations, ref(ba1), ref(ba2), 1.0};

    // transaction
    {
        lock_guard<BankAccount> lk{ba1};
        ba1.deposit(100.0);
        ba1.withdraw(2.0);
    }

    thd1.join();
    thd2.join();
    thd3.join();
    thd4.join();

    cout << "\n--------------------\n" << endl;
    ba1.print();
    ba2.print();
}
