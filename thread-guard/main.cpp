#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds delay)
{
    cout << "Start THD#" << id << endl;

    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;

        this_thread::sleep_for(delay);
    }

    cout << "End of THD#" << id << endl;
}

void may_throw()
{
    throw std::runtime_error("Error#13");
}

class ThreadJoinGuard
{
    thread& thd_;
public:
    ThreadJoinGuard(thread& thd) : thd_{thd}
    {}

    ThreadJoinGuard(const ThreadJoinGuard&) = delete;
    ThreadJoinGuard& operator=(const ThreadJoinGuard&) = delete;

    ~ThreadJoinGuard()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

enum class ThreadDtorAction { join, detach };

class GuardedThread
{
    ThreadDtorAction action_;
    thread thd_;

public:
    GuardedThread(ThreadDtorAction action, thread&& thd)
        : action_{action}, thd_{move(thd)}
    {}

    template <typename... Args>
    GuardedThread(ThreadDtorAction action, Args&&... args)
        : action_{action}, thd_{forward<Args>(args)...}
    {
    }

    GuardedThread(const GuardedThread&) = delete;
    GuardedThread& operator=(const GuardedThread&) = delete;

    GuardedThread(GuardedThread&&) = default;
    GuardedThread& operator=(GuardedThread&&) = default;

    ~GuardedThread()
    {
        if (thd_.joinable())
        {
            if (action_ == ThreadDtorAction::join)
                thd_.join();
            else
                thd_.detach();
        }
    }

    thread& get()
    {
        return thd_;
    }
};

int main() try
{
    // # version 1
    thread thd1{&background_work, 1, 100ms};
    ThreadJoinGuard thd1_guard{thd1};

    GuardedThread guarded_thd2(ThreadDtorAction::join,
                               thread(&background_work, 2, 200ms));

    GuardedThread guarded_thd3(ThreadDtorAction::detach,
                               &background_work, 1, 200ms);

    GuardedThread guarded_thd4  = move(guarded_thd3);

    auto& thd2 = guarded_thd2.get();
    cout << "thd2 id = " << thd2.get_id() << endl;

    may_throw();
}
catch(const exception& e)
{
    cout << "Caught exception: " << e.what() << endl;
}
