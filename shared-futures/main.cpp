#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>

using namespace std;

using FileContent = string;

FileContent download_file(const string& url)
{
    cout << "Download has started... " << url << endl;

    this_thread::sleep_for(2s);

    return "Content of file: " + url;
}

shared_future<FileContent> download_file_async(const string& url)
{
    packaged_task<FileContent()> task{[url] { return download_file(url); } };
    shared_future<FileContent> sf(task.get_future());
    thread thd{move(task)};
    thd.detach();

    return sf;

    //auto future_content = async(launch::async, &download_file, url);
    //return future_content.share();
}

void process_file(shared_future<FileContent> content)
{
    cout << "Processing: " << content.get() << " in a thread#" <<  this_thread::get_id() << endl;
}

int main()
{
    download_file_async("http://other.com");

    shared_future<FileContent> file_content = download_file_async("http://thing.com");

    thread thd1 {&process_file, file_content};
    thread thd2 {[file_content] {
            cout << "From lambda..." << endl;
            process_file(file_content);
    }};

    thd1.join();
    thd2.join();
}
