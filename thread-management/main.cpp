#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void hello(unsigned int id, chrono::milliseconds interval)
{
    string text = "Hello Concurrent World";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c;
        this_thread::sleep_for(interval);
        cout << endl;
    }

    cout << "END of THD#" << id << endl;
}

class BackgroundWork
{
    const int id_;
public:
    BackgroundWork(int id) : id_{id}
    {}

    void operator()(chrono::milliseconds interval)
    {
        for(int i = 0; i < 10; ++i)
        {
            cout << "BW#" << id_ << ": " << i << endl;
            this_thread::sleep_for(interval);
        }

        cout << "BW#" << id_ << " is finished..." << endl;
    }
};

template <typename Container>
void print(const Container& cont, const std::string& prefix)
{
    cout << prefix << ": [ ";
    for(const auto& item : cont)
        cout << item << " ";
    cout << "]" << endl;
}

class Buffer
{
    vector<int> buffer_;
public:
    void assign(const vector<int>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    vector<int> data() const
    {
        return buffer_;
    }
};

int main()
{   
    thread thd1;

    cout << "thd1: " << thd1.get_id() << endl;

    thread thd2(&hello, 2, 100ms);
    thread thd3(&hello, 3, 200ms);
    thd3.detach();

    BackgroundWork bw{1};
    thread thd4{bw, 200ms};
    thread thd5{BackgroundWork{2}, 300ms};

    auto interval = 500ms;

    thread thd6{[interval]{ hello(4, interval); }};

    hello(1, 50ms);

    thd2.join();
    thd4.join();
    thd5.join();
    thd6.join();

    cout << "\n---------------------------\n";

    vector<int> vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    Buffer buff1;
    Buffer buff2;

    thread thd7{[&] { buff1.assign(vec); }};
    thread thd8{bind(&Buffer::assign, ref(buff2), cref(vec))};

    thd7.join();
    thd8.join();

    print(buff1.data(), "buff1");
    print(buff2.data(), "buff2");
}
