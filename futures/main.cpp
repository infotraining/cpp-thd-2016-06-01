#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>

using namespace std;

void save_to_file(const string& filename)
{
    cout << "Saving a file: " << filename << endl;

    this_thread::sleep_for(2s);
}

int calculate_square(int x)
{
    cout << "Starting calculation for " << x << endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    if (x == 13)
        throw std::runtime_error("Error#13");

    return x * x;
}

void may_throw(int x)
{
    if (x % 2 == 0)
        throw runtime_error("Error#2");
}

class SquareCalculator
{
    promise<int> promise_;
public:
    void operator()(int x)
    {
        cout << "Start of calculation in SquareCalculator: " << x << endl;

        this_thread::sleep_for(2s);

        try
        {
            may_throw(x);
        }
        catch(...)
        {
            auto ex = current_exception();
            promise_.set_exception(ex);
            return;
        }

        promise_.set_value(x * x);
    }

    future<int> get_future()
    {
        return promise_.get_future();
    }
};

int main()
{
    // 1st way - package_task
    packaged_task<int()> pt1 { [] { return calculate_square(12); } };
    packaged_task<int(int)> pt2{&calculate_square};
    packaged_task<void()> pt3 { [] { save_to_file("log.txt"); } };

    future<int> f1 = pt1.get_future();
    future<int> f2 = pt2.get_future();
    future<void> f3 = pt3.get_future();

    thread thd1{move(pt1)};
    thread thd2{move(pt2), 13};
    thread thd3{move(pt3)};

    while (f1.wait_for(chrono::milliseconds(100)) != future_status::ready)
    {
        cout << "main is waiting for a result" << endl;
    }

    try
    {
        cout << "f1 = " << f1.get() << endl;
        cout << "f2 = " << f2.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Caught " << e.what() << endl;
    }

    f3.wait();
    cout << "File is saved" << endl;

    thd1.join();
    thd2.join();
    thd3.join();

    // 2nd way - promises
    SquareCalculator calc;

    future<int> f4 = calc.get_future();

    thread thd4{ref(calc), 28};

    try
    {
        cout << "Result: " << f4.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Caught " << e.what() << endl;
    }

    thd4.join();


    // 3rd
    future<int> f5 = async(launch::deferred, &calculate_square, 90);

    vector<future<int>> future_squares;

    for(int i = 100; i < 120; ++i)
    {
        future_squares.push_back(async(launch::async, &calculate_square, i));
    }

    for(auto& f : future_squares)
        cout << "Result: " << f.get() << endl;

    cout << "----------------\n";

    cout << "90^2 = " << f5.get() << endl;

    cout << "----------------\n";

    // pseudo-async
    async(launch::async, &save_to_file, "file1.txt");
    async(launch::async, &save_to_file, "file2.txt");
    async(launch::async, &save_to_file, "file3.txt");
}


