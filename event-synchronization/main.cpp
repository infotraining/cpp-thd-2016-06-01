#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <atomic>
#include <mutex>
#include <condition_variable>

using namespace std;

namespace naive_impl
{
    class Data
    {
        vector<int> data_;
        bool is_ready = false;
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), []{ return rand() % 100; });

            is_ready = true;

            cout << "End of reading..." << endl;
        }

        void process(int id)
        {
            while(!is_ready) {}

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << "; Sum: " << sum << endl;
        }
    };
}

namespace atomic_impl
{
    class Data
    {
        vector<int> data_;
        atomic<bool> is_ready{false};
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), []{ return rand() % 100; });  // A

            is_ready.store(true, memory_order_release); // ++++++++++++++++++++++++  // B

            cout << "End of reading..." << endl;
        }

        void process(int id)
        {
            while(!is_ready.load(memory_order_acquire)) {}  // +++++++++++++++++++++++ C
            {

                long sum = accumulate(data_.begin(), data_.end(), 0L);  // D

                cout << "Id: " << id << "; Sum: " << sum << endl;
            }
        }
    };
}

namespace condition_variables_impl
{
    class Data
    {
        vector<int> data_;
        bool is_ready = false;
        mutex mtx;
        condition_variable cv_ready;
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), []{ return rand() % 100; });

            {
                lock_guard<mutex> lk{mtx};
                is_ready = true;
            }

            cv_ready.notify_all();


            cout << "End of reading..." << endl;
        }

        void process(int id)
        {
            unique_lock<mutex> lk{mtx};
            cv_ready.wait(lk, [this]{ return is_ready; });

            lk.unlock();

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << "; Sum: " << sum << endl;
        }
    };
}

int main()
{
    using namespace condition_variables_impl;

    Data data;

    thread thd1 { [&]{ data.read(); } };
    thread thd2 { [&]{ data.process(1); } };
    thread thd3 { [&]{ data.process(1); } };

    thd1.join();
    thd2.join();
    thd3.join();
}
