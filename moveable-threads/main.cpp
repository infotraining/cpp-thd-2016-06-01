#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds delay)
{
    cout << "Start THD#" << id << endl;

    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;

        this_thread::sleep_for(delay);
    }

    cout << "End of THD#" << id << endl;
}

thread create_thread(int id)
{
    thread thd{&background_work, id, 200ms};

    return thd;
}


int main()
{
    cout << "hardware threads count: " << thread::hardware_concurrency() << endl;

    thread thd1 = create_thread(1);
    thread thd2 = create_thread(2);

    vector<thread> threads(2);

    threads[0] = move(thd1);
    threads[1] = move(thd2);

    thread thd3{&background_work, 3, 300ms};

    threads.push_back(move(thd3));
    threads.push_back(thread{&background_work, 4, 400ms});
    threads.emplace_back(&background_work, 5, 500ms);

    for(auto& thd : threads)
        thd.join();
}
