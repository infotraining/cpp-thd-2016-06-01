#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw std::runtime_error("Error#13 from THD#" + to_string(id));
}

void background_work(int id, int count, chrono::milliseconds delay, exception_ptr& eptr)
{
    cout << "Start THD#" << id << endl;

    try
    {
        for(int i = 0; i < count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;

            this_thread::sleep_for(delay);

            may_throw(id, i);
        }
    }
    catch(...)
    {
        cout << "Exception caught in background_work..." << endl;
        eptr = current_exception(); // storing exception
    }

    cout << "End of THD#" << id << endl;
}

int main()
{
    exception_ptr eptr;

    try
    {
        thread thd1{&background_work, 1, 20, 200ms, ref(eptr)};
        thd1.join();

        // handling possible exception
        if (eptr)
        {
            try
            {
                rethrow_exception(eptr);
            }
            catch(const runtime_error& e)
            {
                cout << "Caught exception in main: " << e.what() << endl;
            }
        }
    }
    catch(const exception& e)
    {
        cout << "Caught exception: " << e.what() << endl;
    }

    const int no_of_threads = 4;

    vector<thread> thds(no_of_threads);
    vector<exception_ptr> thd_excpts(no_of_threads);

    for(int i = 0; i < no_of_threads; ++i)
        thds[i] = thread(&background_work, i+1, (i+1) * 10, 100ms, ref(thd_excpts[i]));

    for(auto& thd : thds)
        thd.join();


    for(auto& e : thd_excpts)
    {
        if (e)
        {
            try
            {
                rethrow_exception(e);
            }
            catch(const runtime_error& ex)
            {
                cout << "Caught: " << ex.what() << endl;
            }
        }
    }
}
